# Airport Intership Spring App
Server Application for returning flight and flight data
Returned data:
- Weight of cargo and cargo for the flight
- Number of flights arriving and departing from the airport
- Number of arriving and departing baggage items from the airport

### Technologie
* Spring Boot
* Spring Data JPA 
* H2 Database
* JUnit
* Mockito
* Joda-Time

### Endpointy

#### Back baggage and cargo weight in Kg and Lb unit
```
GET /api/getBaggageAndCargoWeightForFlight
REQUEST PARAM int flightNumber 
              String date

FULL REQUEST WITH PARAM /api/getBaggageAndCargoWeightForFlight?flightNumber=9893&date=2019-11-17T05:12:48-01:00

Back Body Request JSON
{
    "cargoWeightKg": 1845.78291769,
    "cargoWeightLb": 4069.2547719489717,
    "baggageWeightKg": 1771.8680324,
    "baggageWeightLb": 3906.3003438839323,
    "totalWeightKg": 3617.65095009,
    "totalWeightLb": 7975.5551158329035
}

```

#### Returning the number of arriving and departing flights, the airport and the number of arriving and departing baggage pieces
```
GET /api/getAirportInformation
REQUEST PARAM String airportCode 
              String date

FULL REQUEST WITH PARAM /api/getAirportInformation?airportCode=KRK&date=2019-11-17T05:12:48-01:00

Back Body Request JSON
{
    "numberFlightsDeparture": 2,
    "numberFlightsArriving": 1,
    "numberPiecesBaggageArriving": 3207,
    "numberPiecesBaggageDeparting": 500
}
```

