package com.example.internship.util;

import com.example.internship.entity.Baggage;
import com.example.internship.entity.Cargo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
class WeightCalculatorTest {

    private WeightCalculator weightCalculator;


    @BeforeEach
    void setUp() {

        List<Baggage> baggies = new ArrayList<>();
        baggies.add(new Baggage(0L,1,220,"lb",450));
        baggies.add(new Baggage(0L,2,20,"kg",100));
        baggies.add(new Baggage(0L,3,120,"lb",500));

        List<Cargo> cargos = new ArrayList<>();
        cargos.add(new Cargo(0L,1,100,"kg",300));
        cargos.add(new Cargo(0L,1,100,"kg",300));


        this.weightCalculator = new WeightCalculator(baggies,cargos);
    }

    @Test
    void get_Baggage_Weight_In_Kg_Test() {

        //GIVEN
            double testResult = 174.2214058;
        //WHEN
            double result = this.weightCalculator.getBaggageWeightInKg();
        //TAKE
            assertEquals(result,testResult,2);
            assertNotEquals(result,testResult+100,2);

    }

    @Test
    void get_Baggage_Weight_In_Lb_Test() {
        //GIVEN
        double testResult = 384.0924521148792;
        //WHEN
        double result = this.weightCalculator.getBaggageWeightInLb();
        //TAKE
        assertEquals(result,testResult,2);
        assertNotEquals(result,testResult+100,2);
    }

    @Test
    void get_Cargo_Weight_In_Kg_Test() {
        //GIVEN
        double testResult = 200.0;
        //WHEN
        double result = this.weightCalculator.getCargoWeightInKg();
        //TAKE
        assertEquals(result,testResult,2);
        assertNotEquals(result,testResult+100,2);
    }

    @Test
    void get_Cargo_Weight_In_Lb_Test() {
        //GIVEN
        double testResult = 440.92452399999996;
        //WHEN
        double result = this.weightCalculator.getCargoWeightInLb();
        //TAKE
        assertEquals(result,testResult,2);
        assertNotEquals(result,testResult+100,2);
    }
}
