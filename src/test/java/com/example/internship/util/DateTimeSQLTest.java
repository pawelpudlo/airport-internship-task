package com.example.internship.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Timestamp;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
class DateTimeSQLTest {

    private DateTimeSQL dateTimeSQL;

    @BeforeEach
    void setUp() {
        this.dateTimeSQL = new DateTimeSQL();
    }

    @Test
    void get_Timestamp_Date_Test() {

        //WHEN
        Timestamp date = Timestamp.valueOf("2019-11-17 07:12:48");
        Timestamp result = this.dateTimeSQL.getTimestampDate("2019-11-17T05:12:48-01:00");
        Timestamp badResult = this.dateTimeSQL.getTimestampDate("2019-11-17T05:12:48");
        //THEN

        assertEquals(date,result);
        assertNotEquals(date,badResult);

    }

}
