package com.example.internship.util;

import com.example.internship.entity.Baggage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
class PiecesCalculatorTest {

    private PiecesCalculator piecesCalculator;



    @BeforeEach
    void setUp() {
        this.piecesCalculator = new PiecesCalculator();
    }

    @Test
    void get_Number_Pieces_Baggies_Test() {

        //GIVEN
        List<Baggage> baggies = new ArrayList<>();
        baggies.add(new Baggage(0L,1,220,"lb",450));
        baggies.add(new Baggage(0L,2,20,"kg",100));
        baggies.add(new Baggage(0L,3,120,"lb",500));
        //WHEN

        int result = piecesCalculator.getNumberPiecesBaggies(baggies);

        //TAKE

        assertEquals(result,1050);
        assertNotEquals(result,2000);



    }
}
