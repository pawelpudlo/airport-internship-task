package com.example.internship.controller;

import com.example.internship.service.AirportService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(webEnvironment =
SpringBootTest.WebEnvironment.DEFINED_PORT)
@AutoConfigureMockMvc
class AirportControllerTest {


    @Autowired
    private MockMvc mockMvc;

    @Mock
    private AirportService airportService;

    @BeforeEach
    public void setUp() {
    }

    @Test
    public void get_Weight_Flight_Test() throws Exception {
        //given
        final String baseUrl = createURL("/getBaggageAndCargoWeightForFlight?flightNumber=8495&date=2015-10-11T08:43:48-02:00");

        //take
        this.mockMvc.perform(get(baseUrl))
                .andDo(print()).andExpect(status().isOk());

    }


    @Test
    public void get_Numbers_Airport_Test() throws Exception{
        //given
        final String baseUrl = createURL("/getAirportInformation?airportCode=KRK&date=2019-11-17T05:12:48-01:00");

        //take
        this.mockMvc.perform(get(baseUrl))
                .andDo(print()).andExpect(status().isOk());
    }

    private String createURL(String uri) {
        return "http://localhost:8080/api" + uri;
    }
}
