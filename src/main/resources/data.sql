
DROP TABLE IF EXISTS flight;
DROP TABLE IF EXISTS cargo;
DROP TABLE IF EXISTS baggage;

CREATE TABLE flight (
                        flight_id LONG PRIMARY KEY,
                        flight_number INTEGER NOT NULL,
                        departure_airport_IATA_code VARCHAR(3) NOT NULL,
                        arrival_airport_IATA_code VARCHAR(3) NOT NULL,
                        departure_date DATETIME2 NOT NULL
);

CREATE TABLE cargo (
                       record_id LONG PRIMARY KEY NOT NULL,
                       flight_id LONG NOT NULL,
                       cargo_id INTEGER NOT NULL,
                       weight INTEGER  NOT NULL,
                       weight_unit VARCHAR(2),
                       pieces INTEGER NOT NULL
);

CREATE TABLE baggage (
                         record_id LONG PRIMARY KEY NOT NULL,
                         flight_id LONG NOT NULL,
                         baggage_id INTEGER NOT NULL,
                         weight INTEGER  NOT NULL,
                         weight_unit VARCHAR(2),
                         pieces INTEGER NOT NULL
);



INSERT INTO flight (flight_id, flight_number, departure_airport_iata_code, arrival_airport_IATA_code, departure_date)
VALUES (0,2463,'YYT','GDN','2016-04-06T10:02:23-02:00'),
       (1,1570,'ANC','KRK','2016-03-19T06:24:53-01:00'),
       (2,8495,'YYT','MIT','2015-10-11T08:43:48-02:00'),
       (3,9892,'KRK','ANC','2019-11-17T05:12:48-01:00'),
       (4,9893,'KRK','MIT','2019-11-17T05:12:48-01:00'),
       (5,9894,'ANC','KRK','2019-11-17T05:12:48-01:00');

INSERT INTO cargo (record_id,flight_id, cargo_id, weight, weight_unit, pieces)
VALUES (1,0,0,10,'lb',416),
       (2,0,1,156,'lb',601),
       (3,0,2,217,'kg',441),
       (4,0,3,95,'lb',553),
       (5,1,0,183,'lb',129),
       (6,1,1,167,'kg',653),
       (7,1,2,969,'kg',422),
       (8,2,0,12,'kg',70),
       (9,2,1,100,'lb',616),
       (10,2,2,900,'kg',281),
       (11,3,0,794,'lb',340),
       (12,3,1,873,'kg',890),
       (13,3,2,361,'kg',126),
       (14,3,3,366,'kg',405),
       (15,4,0,318,'kg',323),
       (16,4,1,688,'kg',491),
       (17,4,2,823,'kg',858),
       (18,4,3,37,'lb',564),
       (19,5,0,67,'kg',300),
       (20,5,1,197,'lb',200);

INSERT INTO baggage (record_id,flight_id,baggage_id,weight, weight_unit, pieces)
VALUES (1,0,0,353,'lb',299),
       (2,0,1,474,'lb',332),
       (3,0,2,340,'lb',141),
       (4,0,3,883,'kg',210),
       (5,0,4,407,'lb',400),
       (6,0,5,222,'kg',10),
       (7,0,6,713,'kg',800),
       (8,1,0,402,'kg',92),
       (9,1,1,337,'lb',459),
       (10,1,2,874,'kg',751),
       (11,1,3,497,'kg',340),
       (12,1,4,32,'lb',88),
       (13,1,5,476,'kg',461),
       (14,1,6,504,'kg',496),
       (15,2,0,782,'lb',448),
       (16,2,1,698,'lb',130),
       (17,2,2,624,'kg',83),
       (18,2,3,969,'lb',546),
       (19,2,4,61,'kg',890),
       (20,2,5,797,'kg',160),
       (21,3,0,956,'kg',550),
       (22,3,1,340,'lb',685),
       (23,3,2,248,'lb',589),
       (24,3,3,845,'kg',224),
       (25,4,0,780,'kg',288),
       (26,4,1,520,'lb',70),
       (27,4,2,756,'kg',801),
       (28,5,0,467,'lg',300),
       (29,5,1,297,'kb',200);

