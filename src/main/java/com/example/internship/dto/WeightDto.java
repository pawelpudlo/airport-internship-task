package com.example.internship.dto;

public class WeightDto {

    private double cargoWeightKg;
    private double cargoWeightLb;
    private double baggageWeightKg;
    private double baggageWeightLb;
    private double totalWeightKg;
    private double totalWeightLb;

    public WeightDto(double cargoWeightKg, double cargoWeightLb, double baggageWeightKg, double baggageWeightLb, double totalWeightKg, double totalWeightLb) {
        this.cargoWeightKg = cargoWeightKg;
        this.cargoWeightLb = cargoWeightLb;
        this.baggageWeightKg = baggageWeightKg;
        this.baggageWeightLb = baggageWeightLb;
        this.totalWeightKg = totalWeightKg;
        this.totalWeightLb = totalWeightLb;
    }

    public double getCargoWeightKg() {
        return cargoWeightKg;
    }

    public void setCargoWeightKg(double cargoWeightKg) {
        this.cargoWeightKg = cargoWeightKg;
    }

    public double getCargoWeightLb() {
        return cargoWeightLb;
    }

    public void setCargoWeightLb(double cargoWeightLb) {
        this.cargoWeightLb = cargoWeightLb;
    }

    public double getBaggageWeightKg() {
        return baggageWeightKg;
    }

    public void setBaggageWeightKg(double baggageWeightKg) {
        this.baggageWeightKg = baggageWeightKg;
    }

    public double getBaggageWeightLb() {
        return baggageWeightLb;
    }

    public void setBaggageWeightLb(double baggageWeightLb) {
        this.baggageWeightLb = baggageWeightLb;
    }

    public double getTotalWeightKg() {
        return totalWeightKg;
    }

    public void setTotalWeightKg(double totalWeightKg) {
        this.totalWeightKg = totalWeightKg;
    }

    public double getTotalWeightLb() {
        return totalWeightLb;
    }

    public void setTotalWeightLb(double totalWeightLb) {
        this.totalWeightLb = totalWeightLb;
    }
}
