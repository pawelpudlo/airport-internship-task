package com.example.internship.dto;

public class AirportDto {

    private int numberFlightsDeparture;

    private int numberFlightsArriving;

    private int numberPiecesBaggageArriving;

    private int numberPiecesBaggageDeparting;

    public AirportDto(int numberFlightsDeparture, int numberFlightsArriving, int numberPiecesBaggageArriving, int numberPiecesBaggageDeparture) {
        this.numberFlightsDeparture = numberFlightsDeparture;
        this.numberFlightsArriving = numberFlightsArriving;
        this.numberPiecesBaggageArriving = numberPiecesBaggageArriving;
        this.numberPiecesBaggageDeparting = numberPiecesBaggageDeparture;
    }

    public int getNumberFlightsDeparture() {
        return numberFlightsDeparture;
    }

    public void setNumberFlightsDeparture(int numberFlightsDeparture) {
        this.numberFlightsDeparture = numberFlightsDeparture;
    }

    public int getNumberFlightsArriving() {
        return numberFlightsArriving;
    }

    public void setNumberFlightsArriving(int numberFlightsArriving) {
        this.numberFlightsArriving = numberFlightsArriving;
    }

    public int getNumberPiecesBaggageArriving() {
        return numberPiecesBaggageArriving;
    }

    public void setNumberPiecesBaggageArriving(int numberPiecesBaggageArriving) {
        this.numberPiecesBaggageArriving = numberPiecesBaggageArriving;
    }

    public int getNumberPiecesBaggageDeparting() {
        return numberPiecesBaggageDeparting;
    }

    public void setNumberPiecesBaggageDeparting(int numberPiecesBaggageDeparting) {
        this.numberPiecesBaggageDeparting = numberPiecesBaggageDeparting;
    }
}
