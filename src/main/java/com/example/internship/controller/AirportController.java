package com.example.internship.controller;

import com.example.internship.dto.AirportDto;
import com.example.internship.dto.WeightDto;
import com.example.internship.service.AirportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/api")
public class AirportController {

    private final AirportService airportService;

    @Autowired
    public AirportController(AirportService airportService) {
        this.airportService = airportService;
    }

    @GetMapping(value = "/getBaggageAndCargoWeightForFlight")
    public ResponseEntity<WeightDto> getWeightFlight(@RequestParam(value = "flightNumber", required = false) int flightNumber,
                                                     @RequestParam(value = "date", required = false) String date) {

        return new ResponseEntity<>(airportService.getBaggageWeightFlight(flightNumber,date), HttpStatus.OK);
    }

    @GetMapping(value = "/getAirportInformation")
    public ResponseEntity<AirportDto> getNumbersAirport(@RequestParam(value = "airportCode", required = false) String airportCode,
                                                        @RequestParam(value = "date", required = false) String date) {

        return new ResponseEntity<>(airportService.getNumberPlaceAndBaggageFlight(airportCode,date), HttpStatus.OK);
    }
}
