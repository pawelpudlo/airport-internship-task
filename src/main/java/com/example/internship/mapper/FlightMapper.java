package com.example.internship.mapper;

import com.example.internship.dto.AirportDto;
import com.example.internship.entity.Baggage;
import com.example.internship.util.PiecesCalculator;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FlightMapper {

    public AirportDto mapToDto(List<Baggage> departureBaggies, List<Baggage> arrivalsBaggies, int departureFlights, int arrivalsFlights){
        PiecesCalculator piecesCalculator = new PiecesCalculator();

        int departureNumberBaggies = piecesCalculator.getNumberPiecesBaggies(departureBaggies);

        int arrivalsNumberBaggies = piecesCalculator.getNumberPiecesBaggies(arrivalsBaggies);


        return new AirportDto(departureFlights,arrivalsFlights,departureNumberBaggies,arrivalsNumberBaggies);
    }


}

