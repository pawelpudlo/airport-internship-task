package com.example.internship.mapper;

import com.example.internship.dto.WeightDto;
import com.example.internship.entity.Baggage;
import com.example.internship.entity.Cargo;
import com.example.internship.util.WeightCalculator;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BaggageAndCargoWeightMapper {


    public WeightDto mapToDto(List<Baggage> baggies, List<Cargo> cargos){

        WeightCalculator weightCalculator = new WeightCalculator(baggies,cargos);

        double baggageWeightLb = weightCalculator.getBaggageWeightInLb();
        double baggageWeightKg = weightCalculator.getBaggageWeightInKg();

        double cargoWeightLb = weightCalculator.getCargoWeightInLb();
        double cargoWeightKg = weightCalculator.getCargoWeightInKg();

        double totalWeightLb = cargoWeightLb + baggageWeightLb;
        double totalWeightKg = cargoWeightKg + baggageWeightKg;

        return new WeightDto(
                cargoWeightKg,
                cargoWeightLb,
                baggageWeightKg,
                baggageWeightLb,
                totalWeightKg,
                totalWeightLb);
    }







}
