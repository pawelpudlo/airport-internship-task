package com.example.internship.util;

import com.example.internship.entity.Baggage;
import com.example.internship.entity.Cargo;

import java.util.List;

public class WeightCalculator {

    private List<Baggage> baggies;
    private List<Cargo> cargos;

    public WeightCalculator(List<Baggage> baggies, List<Cargo> cargos) {
        this.baggies = baggies;
        this.cargos = cargos;
    }

    public double getBaggageWeightInKg(){
        double weight = 0;
        for (Baggage baggies:this.baggies) {
            if(baggies.getWeightUnit().equals("kg")){
                weight += baggies.getWeight();
            }else{
                weight += baggies.getWeight() * 0.45359237;
            }
        }
        return weight;
    }

    public double getBaggageWeightInLb(){
        return getBaggageWeightInKg() * 2.20462262;
    }

    public double getCargoWeightInKg(){
        double weight = 0;
        for (Cargo cargos:this.cargos) {
            if(cargos.getWeightUnit().equals("kg")){
                weight += cargos.getWeight();
            }else{
                weight += cargos.getWeight() * 0.45359237;
            }
        }
        return weight;
    }

    public double getCargoWeightInLb(){
        return getCargoWeightInKg() * 2.20462262;
    }

    public List<Baggage> getBaggies() {
        return baggies;
    }

    public void setBaggies(List<Baggage> baggies) {
        this.baggies = baggies;
    }

    public List<Cargo> getCargos() {
        return cargos;
    }

    public void setCargos(List<Cargo> cargos) {
        this.cargos = cargos;
    }
}
