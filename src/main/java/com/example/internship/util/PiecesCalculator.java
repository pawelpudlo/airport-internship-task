package com.example.internship.util;

import com.example.internship.entity.Baggage;

import java.util.List;

public class PiecesCalculator {


    public PiecesCalculator() {

    }

    public int getNumberPiecesBaggies(List<Baggage> baggiesList){
        int pieces=0;

        for (Baggage baggies:baggiesList) {
            pieces += baggies.getPieces();
        }
        return pieces;
    }

}
