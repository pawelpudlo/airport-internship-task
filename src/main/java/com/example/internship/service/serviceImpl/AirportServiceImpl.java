package com.example.internship.service.serviceImpl;

import com.example.internship.dto.AirportDto;
import com.example.internship.dto.WeightDto;
import com.example.internship.entity.Baggage;
import com.example.internship.entity.Cargo;
import com.example.internship.mapper.BaggageAndCargoWeightMapper;
import com.example.internship.mapper.FlightMapper;
import com.example.internship.repository.BaggageRepository;
import com.example.internship.repository.CargoRepository;
import com.example.internship.repository.FlightRepository;
import com.example.internship.service.AirportService;
import com.example.internship.util.DateTimeSQL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service
public class AirportServiceImpl implements AirportService {

    private final BaggageRepository baggageRepository;
    private final CargoRepository cargoRepository;
    private final FlightRepository flightRepository;
    private final BaggageAndCargoWeightMapper baggageAndCargoWeightMapper;
    private final FlightMapper flightMapper;

    @Autowired
    public AirportServiceImpl(BaggageRepository baggageRepository,
                              CargoRepository cargoRepository,
                              FlightRepository flightRepository,
                              BaggageAndCargoWeightMapper baggageAndCargoWeightMapper,
                              FlightMapper flightMapper) {
        this.baggageRepository = baggageRepository;
        this.cargoRepository = cargoRepository;
        this.flightRepository = flightRepository;
        this.baggageAndCargoWeightMapper = baggageAndCargoWeightMapper;
        this.flightMapper = flightMapper;

    }

    @Override
    public WeightDto getBaggageWeightFlight(int flightNumber, String date) {

        Timestamp timestampDate = new DateTimeSQL().getTimestampDate(date);

        List<Baggage> baggies = baggageRepository.getBaggageListByFlightNumber(flightNumber,timestampDate);
        List<Cargo> cargos = cargoRepository.getCargoListByFlightNumber(flightNumber,timestampDate);
        return baggageAndCargoWeightMapper.mapToDto(baggies,cargos);
    }

    @Override
    public AirportDto getNumberPlaceAndBaggageFlight(String airportCode, String date) {

        Timestamp timestampDate = new DateTimeSQL().getTimestampDate(date);

        List<Baggage> departureBaggies = baggageRepository.getDepartureBaggageListByAirportCode(airportCode,timestampDate);
        List<Baggage> arrivalBaggies = baggageRepository.getArrivalBaggageListByAirportCode(airportCode,timestampDate);
        int departureFlights = flightRepository.getNumberDepartureFlight(airportCode,timestampDate);
        int arrivalsFlights = flightRepository.getNumberArrivalFlight(airportCode,timestampDate);

        return flightMapper.mapToDto(departureBaggies,arrivalBaggies,departureFlights,arrivalsFlights);
    }
}
