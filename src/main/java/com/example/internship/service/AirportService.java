package com.example.internship.service;

import com.example.internship.dto.AirportDto;
import com.example.internship.dto.WeightDto;

public interface AirportService {

    WeightDto getBaggageWeightFlight(int flightNumber, String date);

    AirportDto getNumberPlaceAndBaggageFlight(String airportCode, String date);

}
