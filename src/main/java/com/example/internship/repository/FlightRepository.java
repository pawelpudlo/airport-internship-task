package com.example.internship.repository;

import com.example.internship.entity.Baggage;
import com.example.internship.entity.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface FlightRepository extends CrudRepository<Flight,Long> {

    @Query("SELECT COUNT(flight) FROM Flight flight WHERE flight.departureAirportCode=:codeAirport AND flight.departureDate=:date")
    int getNumberDepartureFlight(@Param("codeAirport") String codeAirport,
                                 @Param("date") Timestamp date);

    @Query("SELECT COUNT(flight) FROM Flight flight WHERE flight.arrivalAirportCode=:codeAirport AND flight.departureDate=:date")
    int getNumberArrivalFlight(@Param("codeAirport") String codeAirport,
                               @Param("date") Timestamp date);

    @Query("select flight.id from Flight flight WHERE flight.arrivalAirportCode=:codeAirport AND flight.departureDate=:date")
    List<Integer> getArrivalFlightId(@Param("codeAirport") String codeAirport,
                                     @Param("date") Timestamp date);

}
