package com.example.internship.repository;

import com.example.internship.entity.Baggage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface BaggageRepository extends JpaRepository<Baggage,Long> {


    @Query("SELECT baggage FROM Baggage baggage,Flight flight " +
            "WHERE baggage.flightId=flight.id " +
            "AND flight.numberFlight=:flightNumber " +
            "AND flight.departureDate=:date")
    List<Baggage> getBaggageListByFlightNumber(@Param("flightNumber") int flightNumber,
                                               @Param("date") Timestamp date);


    @Query("SELECT baggage FROM Baggage baggage, Flight flight " +
            "WHERE flight.id=baggage.flightId " +
            "AND flight.arrivalAirportCode=:codeAirport " +
            "AND flight.departureDate=:date")
    List<Baggage> getArrivalBaggageListByAirportCode(@Param("codeAirport") String codeAirport, @Param("date") Timestamp date);


    @Query("SELECT baggage FROM Baggage baggage, Flight flight " +
            "WHERE flight.id=baggage.flightId " +
            "AND flight.departureAirportCode=:codeAirport " +
            "AND flight.departureDate=:date")
    List<Baggage> getDepartureBaggageListByAirportCode(@Param("codeAirport") String codeAirport, @Param("date") Timestamp date);


}
