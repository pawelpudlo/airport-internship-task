package com.example.internship.repository;

import com.example.internship.entity.Baggage;
import com.example.internship.entity.Cargo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface CargoRepository extends CrudRepository<Cargo,Long> {

    @Query("SELECT cargo FROM Cargo cargo,Flight flight " +
            "WHERE cargo.flightId=flight.id " +
            "AND flight.numberFlight=:flightNumber " +
            "AND flight.departureDate=:date")
    List<Cargo> getCargoListByFlightNumber(@Param("flightNumber") int flightNumber,
                                           @Param("date") Timestamp date);

}
